<?php
//$iUserID = filter_input(INPUT_GET, iUserID, FILTER_SANITIZE_NUMBER_INT);
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/header.php";

$item = $cart->getItemByUser($auth->iUserID);
$rows = $cart->getCartLines($item);
?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>stk pris</th>
                        <th class="text-center">Quantity</th>
                        <th class="text-center">Total</th>
                        <th> </th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($rows as $key => $row):
                        $product = new shopproduct();
                        $product->getProduct($row["iProductID"])
                        ?>
                        <tr id="cartline<?php echo $row["iCartLineID"] ?>">
                            <td class="col-sm-8 col-md-6">
                                <div class="media">
                                    <a class="thumbnail pull-left" href="#">
                                        <img class="media-object"
                                             src="http://icons.iconarchive.com/icons/custom-icon-design/flatastic-2/72/product-icon.png"
                                             style="width: 72px; height: 72px;">
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a
                                                    href="#"><?php echo $product->arrValues["vcTitle"] ?></a></h4>
                                        <h5 class="media-heading">Varenummer:
                                            <?php echo $product->arrValues["vcProductNumber"] ?>
                                        </h5>
                                        <span> <strong><?php echo $product->arrValues["iStock"] ?></strong></span><span
                                                class="text-success"><strong>
                                                stk på lager</strong></span>
                                    </div>
                                </div>
                            </td>

                            <td class="col-sm-1 col-md-1 text-center">
                                <span class="price"><strong><?php echo $product->arrValues["iPrice"] ?>
                                        ,-</strong></span>
                            </td>
                            <td class="col-sm-1 col-md-1" style="text-align: center">
                                <input type="number" class="form-control" id="quantity<?php echo $row["iCartLineID"] ?>"
                                       value="<?php echo $row["iQuantity"] ?>">
                            </td>
                            <td class="col-sm-1 col-md-1 text-center">
                                <strong class="itemtotal"><?php echo $product->iPrice * $row["iQuantity"]; ?>,-</strong>
                            </td>
                            <td class="col-sm-2 col-md-2">
                                <i class="fa fa-refresh btn update" aria-hidden="true"
                                   data-cartlineid="<?php echo $row["iCartLineID"] ?>"></i>


                                <button type="button" class="btn btn-danger remove"
                                        data-cartlineid="<?php echo $row["iCartLineID"] ?>">
                                <span class=" glyphicon glyphicon-remove
                            "></span> Fjern
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <td>  </td>
                    <td>  </td>
                    <td>  </td>
                    <td><h3>Total</h3></td>
                    <td class="text-right"><h3><strong class="total"><?php echo $cart->getCartTotal(); ?>,-</strong>
                        </h3></td>
                    </tr>
                    <tr>
                        <td>  </td>
                        <td>  </td>
                        <td>  </td>
                        <td>
                            <button type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-shopping-cart"></span> Fortsæt med at handle
                            </button>
                        </td>
                        <td>
                            <a href="cartinfo.php" class="btn btn-success">
                                GÅ TIL KASSEN
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";