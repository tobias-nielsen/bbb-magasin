<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 11-09-2017
 * Time: 09:18
 */

$news = new news();
$views = $news->getPopulareNews(6);

?>

<div class="col-sm-4 side-bar">
    <form>
        <input type="search" placeholder="Søg i arkivet...">
        <input class="btn" type="submit">

    </form>

    <div>
        <h4 class="headline">MEST LÆSTE</h4>
        <ul>
            <?php foreach ($views as $key => $row): ?>
                <li><?php echo $row["vcTitle"] ?></li>

            <?php endforeach; ?>
        </ul>
    </div>
    <div class="sponsor">
        <h4 class="headline">SPONSOR</h4>
        <ul class="text-center">
            <li><img src="https://www.placecage.com/c/150/60"
                     class="margin-bot15" alt="user profile image"></li>
            <li><img src="https://www.placecage.com/c/150/60"
                     class="margin-bot15" alt="user profile image"></li>
            <li><img src="https://www.placecage.com/c/150/60"
                     class="margin-bot15" alt="user profile image"></li>
            <li><img src="https://www.placecage.com/c/150/60"
                     class="margin-bot15" alt="user profile image"></li>
            <li><img src="https://www.placecage.com/c/150/60"
                     class="margin-bot15" alt="user profile image"></li>
        </ul>
    </div>
</div>
