<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";


$org = new org();
$org->getOrg(7);
?>
    </div>
    <div class="container padding-zero">
        <footer>
            <div class="row">
                <div class="col-sm-4 padding-zero">
                    <ul>
                        <h4>Adresse</h4>
                        <li><strong>Magasinet Bil, Båd & Bike</strong></li>
                        <li><?php echo $org->vcAddress ?></li>
                        <li><?php echo $org->iZip . " " . $org->vcCity ?></li>
                        <li><?php echo $org->vcCountry ?></li>
                    </ul>
                </div>

                <div class="col-sm-4">
                    <ul class="padding-zero">
                        <h4>Kontakt</h4>
                        <li>Telefon: <?php echo $org->vcPhone ?></li>
                        <li>Fax: <?php echo $org->vcFax ?></li>
                        <li>E-mail: <?php echo $org->vcEmail ?></li>
                    </ul>
                </div>

                <div class="col-sm-4 padding-zero">
                    <form method="POST" action="assets/scripts/newsletter.php">
                        <fieldset><h4>Nyhedsbrev</h4></fieldset>
                        <div class="col-sm-12 padding-zero margin-bot15">
                            <input class="newsletter form-control" type="email" id="vcEmail" required name="vcEmail"
                                   placeholder="E-mailadresse">
                        </div>
                        <div class="col-sm-12 padding-zero">
                            <button class="main-btn" type="submit">TILMELD</button>
                            <button class="main-btn" type="submit">FRAMELD</button>
                        </div>
                    </form>
                </div>
            </div>
        </footer>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/assets/js/functions.js"></script>
    <script src="/assets/js/ajaxFunctions.js"></script>

    </body>
    </html>

<?php
$db->_close();