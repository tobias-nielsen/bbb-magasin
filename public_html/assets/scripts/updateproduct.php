<?php require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/webshop/public_html/assets/incl/init.php";
$iCartLineID = (int)filter_input(INPUT_POST, "iCartLineID", FILTER_SANITIZE_NUMBER_INT);
$iQuantity = (int)filter_input(INPUT_POST, "iQuantity", FILTER_SANITIZE_NUMBER_INT);


$cart->updateProduct($iCartLineID, $iQuantity);

$arrJson = array(
    "iCartLineID" => $iCartLineID,
    "iCartTotal" => $cart->getCartTotal(),
    "productsInCart" => $cart->getCartQuantity()

);

echo json_encode($arrJson);
