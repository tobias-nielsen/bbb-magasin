<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";

$username = filter_input(INPUT_POST, "vcUserName", FILTER_SANITIZE_STRING);

$db = new dbconf();
$db->_connect();

if ($username) {
    $username = $username;
    $sql = "SELECT vcUserName FROM user WHERE iDeleted = 0 AND vcUserName = ?";
    $params = array($username);
    $rows = $db->_fetch_array($sql, $params);

    if (count($rows) !== 0) {
        echo 1;
    } else {
        echo 0;
    }
}
