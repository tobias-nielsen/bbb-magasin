<?php require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/header.php";


$strModuleName = "";

$mode = setMode();

switch (strtoupper($mode)) {

    case "LIST";

        $shopproduct = new shopproduct();
        $rows = $shopproduct->getList();

        ?>
        <!-- Page Content -->
        <div class="container">

            <div class="row">

                <div class="col-md-3">
                    <p class="lead">Webshop</p>
                    <div class="list-group">
                        <a href="#" class="list-group-item">Bukser</a>
                        <a href="#" class="list-group-item">Trøjer</a>
                        <a href="#" class="list-group-item">T-shirts</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <?php foreach ($rows as $key => $row): ?>
                            <a href="<?php echo "shop.php?mode=details&iProductID=" . $row["iProductID"] ?>">

                                <div class="col-sm-4 col-lg-4 col-md-4">
                                    <div class="thumbnail">
                                        <img src="images/<?php echo $row["vcImage1"] ?>" alt="">
                                        <div class="caption">
                                            <h4 class="pull-right"><?php echo $row["iPrice"] ?>,-</h4>
                                            <h4>
                                                <a href="<?php echo "shop.php?mode=details&iProductID=" . $row["iProductID"] ?>"><?php echo $row["vcTitle"] ?></a>
                                            </h4>
                                            <!--     <p><?php /*echo $row["txShortDescription"] */ ?></p>-->


                                            <a class="btn btn-success addtocart"
                                               data-product="<?php echo $row["iProductID"] ?>"> Add to
                                                cart</a>
                                            <input class="form-control quantity" type="number"
                                                   min="1"
                                                   value="1">
                                            <div class="isincart">

                                            </div>


                                        </div>
                                        <div class="ratings">
                                            <p class="pull-right">15 reviews</p>
                                            <p>
                                                <span class="glyphicon glyphicon-star"></span>
                                                <span class="glyphicon glyphicon-star"></span>
                                                <span class="glyphicon glyphicon-star"></span>
                                                <span class="glyphicon glyphicon-star"></span>
                                                <span class="glyphicon glyphicon-star"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>

                        <?php endforeach; ?>

                    </div>

                </div>

            </div>
        </div>
        </div>


        <?php require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";

        break;

    case
    "DETAILS";
        $iProductID = filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT); ?>
        <?php

        $shopproduct = new shopproduct();

        $rows = $shopproduct->getProduct($iProductID);
        ?>


        <div class="container-fluid">
            <div class="content-wrapper">
                <div class="item-container">
                    <div class="container">
                        <div class="col-md-12">
                            <div class="product col-md-3 service-image-left">
                                <img id="item-display"
                                     src="http://www.corsair.com/Media/catalog/product/g/s/gs600_psu_sideview_blue_2.png"
                                     alt="">
                            </div>

                            <div class="container service1-items col-sm-2 col-md-2 pull-left">
                                <a id="item-1" class="service1-item">
                                    <img src="http://www.corsair.com/Media/catalog/product/g/s/gs600_psu_sideview_blue_2.png"
                                         alt="">
                                </a>
                                <a id="item-2" class="service1-item">
                                    <img src="http://www.corsair.com/Media/catalog/product/g/s/gs600_psu_sideview_blue_2.png"
                                         alt="">
                                </a>
                                <a id="item-3" class="service1-item">
                                    <img src="http://www.corsair.com/Media/catalog/product/g/s/gs600_psu_sideview_blue_2.png"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-md-7">
                                <div class="product-title"><?php echo $shopproduct->vcTitle ?></div>
                                <div class="product-desc"><?php echo $shopproduct->txShortDesc ?>
                                </div>
                                <div class="product-rating"><i class="fa fa-star gold"></i> <i
                                            class="fa fa-star gold"></i>
                                    <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i
                                            class="fa fa-star-o"></i></div>
                                <hr>
                                <div class="product-price"><?php echo $shopproduct->iPrice ?>,-</div>
                                <div class="product-stock"><?php echo $shopproduct->iStock ?>&nbsp;left in stock</div>
                                <hr>
                                <div class="btn-group cart">
                                    <button type="button" class="btn btn-success">
                                        Add to cart
                                    </button>
                                </div>
                                <div class="btn-group wishlist">
                                    <button type="button" class="btn btn-danger">
                                        Add to wishlist
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="col-md-12 product-info">
                        <ul id="myTab" class="nav nav-tabs nav_tabs">

                            <li class="active"><a href="#service-one" data-toggle="tab">DESCRIPTION</a></li>
                            <li><a href="#service-two" data-toggle="tab">PRODUCT INFO</a></li>
                            <li><a href="#service-three" data-toggle="tab">REVIEWS</a></li>

                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <?php echo $shopproduct->txShortDesc ?>
                            <div class="tab-pane fade in active" id="service-one">
                                <section class="container product-info">
                                    <?php echo $shopproduct->txLongDesc ?>
                                </section>

                            </div>

                            <!------PRODUCT INFO ------>
                            <div class="tab-pane fade" id="service-two">
                                <section class="container">
                                </section>

                            </div>
                            <!------REVIEWS ------>

                            <div class="tab-pane fade" id="service-three">

                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>


        <?php require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";


        break;

}
