<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

$mode = setMode();

$strModuleName = "product";

switch (strtoupper($mode)) {

    case "LIST";
        $strModuleMode = "Overview";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iProductID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $product = new shopproduct();

        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcTitle" => "Product Name",
            "iPrice" => "Price",
            "iOfferPrice" => "Offer Price",
            "iIsActive" => "Active",
        );

        /* Array for all product rows */
        $arrProducts = array();

        /* List orgs and set editing options */
        foreach ($product->getlist() as $key => $arrValues) {
            $arrValues["iIsActive"] = boolToIcon($arrValues["iIsActive"]);
            $arrValues["opts"] = getIcon("?mode=details&iProductID=" . $arrValues["iProductID"], "eye") .
                getIcon("?mode=edit&iProductID=" . $arrValues["iProductID"], "pencil") .
                getIcon("", "trash", "Slet product", "remove(" . $arrValues["iProductID"] . ")");

            /* Add value row to arrUsers */
            $arrProducts[] = $arrValues;
        }

        /* Call list presenter object with columns (arrColumns) and rows (arrProducts) */
        $p = new listPresenter($arrColumns, $arrProducts);
        echo $p->presentlist();

        sysFooter();
        break;

    case "DETAILS";
        $iProductID = filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();


        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iProductID=" . $iProductID, "Edit Produkt", "btn-success");

        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $product = new shopproduct();
        $product->getproduct($iProductID);


        $arrValues = get_object_vars($product);

        /*Converts date/stamp to readable date */
        $arrValues["iIsActive"] = boolToIcon($arrValues["iIsActive"]);
        $arrValues["daCreated"] = date2local($arrValues["daCreated"]);


        $presenter = new listPresenter($product->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iProductID = (int)filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Details";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "Details", "getUrl('?mode=details&iProductID=" . $iProductID . "'), ", "btn-info");
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iProductID=-1')", "btn-success");
        $arrButtonPanel[] = getButton("button", "Overview", "getUrl('?mode=list')", "btn-primary");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        /* Create class instance and set current org */
        $product = new shopproduct();

        /* Get org if state = update */
        if ($iProductID > 0) {
            $product->getproduct($iProductID);
        }

        /* Get property values */
        $arrValues = get_object_vars($product);


        /* Create presenter instance and set form */
        $form = new formpresenter($product->arrLabels, $product->arrFormElms, $arrValues);
        echo $form->presentForm();

        sysFooter();
        break;

    case "SAVE":
        $product = new shopproduct();

        /*
         * Loop form elements from org class & set org property if exists
         * Otherwise set default value from form elements
         * (Defined in org class)
         */
        foreach ($product->arrFormElms as $field => $arrTypes) {
            $product->$field = filter_input(INPUT_POST, $field, $arrTypes[1], getDefaultValue($arrTypes[3]));
        }

        $product->daStart = makeStamp("daStart");
        $product->daStop = makeStamp("daStop");

        /* Save method*/
        $iProductID = $product->save();
        header("Location: ?mode=details&iProductID=" . $iProductID);
        break;

    case "DELETE":
        $product = new shopproduct();
        $product->iProductID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $product->delete($iProductID);
        header("Location: ?mode=list");
        break;


}

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php"; ?>
<script src="/assets/js/ajaxFunctions.js"></script>
