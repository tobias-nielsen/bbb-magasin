<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

$mode = setMode();

$strModuleName = "news";

switch (strtoupper($mode)) {

    case "LIST";
        $strModuleMode = "Overview";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iNewsID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $news = new news();

        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcTitle" => "Title",
            "iIsActive" => "Active",
        );

        /* Array for all news rows */
        $arrnewss = array();

        /* List orgs and set editing options */
        foreach ($news->getlist() as $key => $arrValues) {
            $arrValues["iIsActive"] = boolToIcon($arrValues["iIsActive"]);
            $arrValues["opts"] = getIcon("?mode=details&iNewsID=" . $arrValues["iNewsID"], "eye") .
                getIcon("?mode=edit&iNewsID=" . $arrValues["iNewsID"], "pencil") .
                getIcon("", "trash", "Slet news", "remove(" . $arrValues["iNewsID"] . ")");

            /* Add value row to arrUsers */
            $arrnewss[] = $arrValues;


        }

        /* Call list presenter object with columns (arrColumns) and rows (arrnewss) */
        $p = new listPresenter($arrColumns, $arrnewss);
        echo $p->presentlist();

        sysFooter();
        break;

    case "DETAILS";
        $iNewsID = filter_input(INPUT_GET, "iNewsID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iNewsID=" . $iNewsID, "Edit news", "btn-success");

        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $news = new news();
        $news->getNews($iNewsID);


        $arrValues = get_object_vars($news);


        $presenter = new listPresenter($news->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iNewsID = (int)filter_input(INPUT_GET, "iNewsID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Details";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "Details", "getUrl('?mode=details&iNewsID=" . $iNewsID . "'),");
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iNewsID=-1')");
        $arrButtonPanel[] = getButton("button", "Overview", "getUrl('?mode=list')");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        /* Create class instance and set current org */
        $news = new news();

        /* Get org if state = update */
        if ($iNewsID > 0) {
            $news->getNews($iNewsID);
        }

        /* Get property values */
        $arrValues = get_object_vars($news);

        /* Get orgs as venues */
        $strSelect = "SELECT iCatID, vcName FROM category WHERE iDeleted = 0 ORDER BY vcName";
        $arrCats = $db->_fetch_array($strSelect);
        /* Add a default value to the selectbox */
        array_unshift($arrCats, array("iCatID" => 0, "vcName" => "Choose Category"));

        $arrValues["iCatID"] = formpresenter::inputSelect("iCatID", $arrCats, $news->iCatID);


        /* Create presenter instance and set form */
        $form = new formpresenter($news->arrLabels, $news->arrFormElms, $arrValues);
        echo $form->presentForm();

        sysFooter();
        break;

    case "SAVE":
        $news = new news();


        foreach ($news->arrFormElms as $field => $arrTypes) {
            $news->$field = filter_input(INPUT_POST, $field, $arrTypes[1], getDefaultValue($arrTypes[3]));
        }


        /* Save method*/
        $iNewsID = $news->save();
        header("Location: ?mode=details&iNewsID=" . $iNewsID);
        break;

    case "DELETE":
        $news = new news();
        $news->iNewsID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $news->delete($iNewsID);
        header("Location: ?mode=list");
        break;


}

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php"; ?>
<script src="/assets/js/ajaxFunctions.js"></script>
