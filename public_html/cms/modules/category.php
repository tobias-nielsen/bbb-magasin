<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

$mode = setMode();

$strModuleName = "Category";

switch (strtoupper($mode)) {

    case "LIST";
        $strModuleMode = "Overview";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iCatID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $category = new category();

        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcName" => "Category Name",
            "txDescription" => "Description",
        );

        /* Array for all category rows */
        $arrcategories = array();

        /* List orgs and set editing options */
        foreach ($category->getlist() as $key => $arrValues) {
            $arrValues["opts"] = getIcon("?mode=details&iCatID=" . $arrValues["iCatID"], "eye") .
                getIcon("?mode=edit&iCatID=" . $arrValues["iCatID"], "pencil") .
                getIcon("", "trash", "Slet category", "remove(" . $arrValues["iCatID"] . ")");

            /* Add value row to arrUsers */
            $arrcategories[] = $arrValues;
        }

        /* Call list presenter object with columns (arrColumns) and rows (arrcategories) */
        $p = new listPresenter($arrColumns, $arrcategories);
        echo $p->presentlist();

        sysFooter();
        break;

    case "DETAILS";
        $iCatID = filter_input(INPUT_GET, "iCatID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iCatID=" . $iCatID, "Edit category", "btn-success");

        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $category = new category();
        $category->getItem($iCatID);


        $arrValues = get_object_vars($category);


        $presenter = new listPresenter($category->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iCatID = (int)filter_input(INPUT_GET, "iCatID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Details";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "Details", "getUrl('?mode=details&iCatID=" . $iCatID . "'),");
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iCatID=-1')");
        $arrButtonPanel[] = getButton("button", "Overview", "getUrl('?mode=list')");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        /* Create class instance and set current org */
        $category = new category();

        /* Get org if state = update */
        if ($iCatID > 0) {
            $category->getItem($iCatID);
        }

        /* Get property values */
        $arrValues = get_object_vars($category);


        /* Create presenter instance and set form */
        $form = new formpresenter($category->arrLabels, $category->arrFormElms, $arrValues);
        echo $form->presentForm();

        sysFooter();
        break;

    case "SAVE":
        $category = new category();


        foreach ($category->arrFormElms as $field => $arrTypes) {
            $category->$field = filter_input(INPUT_POST, $field, $arrTypes[1], getDefaultValue($arrTypes[3]));
        }

        /* Save method*/
        $iCatID = $category->save();
        header("Location: ?mode=details&iCatID=" . $iCatID);
        break;

    case "DELETE":
        $category = new category();
        $category->iCatID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $category->delete($iCatID);
        header("Location: ?mode=list");
        break;


}

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php"; ?>
<script src="/assets/js/ajaxFunctions.js"></script>
