<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

sysHeader();
?>


        <div>
            <h1>Upload your files here</h1>
            <form action="../../upload.php" method="POST" enctype="multipart/form-data">
                <input type="file" name="file">
                <button type="submit" name="submit">Upload File</button>
            </form>
        </div>
<?php

sysFooter();
