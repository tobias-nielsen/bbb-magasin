<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

$mode = setMode();

$strModuleName = "User";

switch (strtoupper($mode)) {

    case "LIST";
        $strModuleMode = "Overview";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iUserID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $user = new user();

        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcUserName" => "Title",
        );

        /* Array for all user rows */
        $arrUsers = array();

        /* List orgs and set editing options */
        foreach ($user->getlist() as $key => $arrValues) {
            $arrValues["opts"] = getIcon("?mode=details&iUserID=" . $arrValues["iUserID"], "eye") .
                getIcon("?mode=edit&iUserID=" . $arrValues["iUserID"], "pencil") .
                getIcon("", "trash", "Slet user", "remove(" . $arrValues["iUserID"] . ")");

            /* Add value row to arrUsers */
            $arrUsers[] = $arrValues;
        }

        /* Call list presenter object with columns (arrColumns) and rows (arrusers) */
        $p = new listPresenter($arrColumns, $arrUsers);
        echo $p->presentlist();

        sysFooter();
        break;

    case "DETAILS";
        $iUserID = filter_input(INPUT_GET, "iUserID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iUserID=" . $iUserID, "Edit bruger", "btn-success");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=setusergroups&iUserID=" . $iUserID, "Vælg grupper", "btn-success");
        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $user = new user();
        $user->getuser($iUserID);

        $arrValues = get_object_vars($user);
        unset($arrValues["vcPassword"]);
        $arrValues["daCreated"] = date2local($arrValues["daCreated"]);


        $presenter = new listPresenter($user->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iUserID = filter_input(INPUT_GET, "iUserID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = ($iUserID > 0) ? "Edit" : "New User";
        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        /* Create class instance and get current user */
        $user = new user();


        if ($iUserID > 0) {
            $user->getuser($iUserID);
        }


        // must be below if statement, else won't get iUserID on update
        $arrValues = get_object_vars($user);


        /* Get orgs as venues */
        $strSelect = "SELECT iOrgID, vcOrgName FROM org WHERE iDeleted = 0 ORDER BY vcOrgName";
        $arrOrgs = $db->_fetch_array($strSelect);
        /* Add a default value to the selectbox */
        array_unshift($arrOrgs, array("iOrgID" => 0, "vcOrgName" => "Vælg venue"));

        $arrValues["iOrgID"] = formpresenter::inputSelect("iOrgID", $arrOrgs, $user->iOrgID);

        /* Create presenter instance and set form */
        $form = new formpresenter($user->arrLabels, $user->arrFormElms, $arrValues);
        echo $form->presentForm();


        sysFooter();
        break;

    case "SAVE":
        $user = new user();


        foreach ($user->arrFormElms as $field => $arrTypes) {
            $user->$field = filter_input(INPUT_POST, $field, $arrTypes[1], getDefaultValue($arrTypes[3]));
        }


        $iUserID = $user->save();
        header("Location: ?mode=details&iUserID=" . $iUserID);
        break;

    case "DELETE":
        $user = new user();
        $user->iUserID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $user->delete();
        header("Location: ?mode=list");
        break;

    case "SETUSERGROUPS":
        $iUserID = filter_input(INPUT_GET, "iUserID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Bruger grupper";
        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("eye", "?mode=details&iUserID=" . $iUserID, "Se gruppe", "btn-primary");
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $user = new user();
        $user->getuser($iUserID);


        $user->arrGroups = array_column($user->arrGroups, "iGroupID");

        // Call new class instance & get all groups from the group object
        $group = new usergroup();
        $rows = $group->getList();


        /* Create arrays for presenter,  */
        $arrLabels = array();
        $arrFormElms = array();
        $arrFormValues = array();

        /*Add user id as hidden value field, as done in user class
         Define form values with user id value
         */
        $arrFormElms["iUserID"] = array("hidden", FILTER_SANITIZE_NUMBER_INT, FALSE, 0);
        $arrFormValues["iUserID"] = $iUserID;

        // Loop rows and build form & checkboxes using form arrays
        foreach ($rows as $key => $values) {
            $field = "groups[" . $values["iGroupID"] . "]";

            /*Takes groupName to use as label */
            $arrLabels[$field] = $values["vcGroupName"];

            /*Define form element & create checkbox */
            $arrFormElms[$field] = array("checkbox", FILTER_SANITIZE_NUMBER_INT, FALSE, 0);

            // Set form values with related group ids

            if (in_array($values["iGroupID"], $user->arrGroups)) {
                $arrFormValues[$field] = $values["iGroupID"];
            }
        }

        $form = new formpresenter($arrLabels, $arrFormElms, $arrFormValues);
        $form->formAction = "saveusergroups";
        echo $form->presentform();

        sysFooter();
        break;

    case "SAVEUSERGROUPS":
        $iUserID = filter_input(INPUT_POST, "iUserID", FILTER_SANITIZE_NUMBER_INT);

        // Delete existing user related groups
        $params = array($iUserID);
        $strDelete = "DELETE FROM usergrouprel WHERE iUserID = ?";
        $db->_query($strDelete, $params);

        // Create array for post filtering
        $args = array(
            "groups" => array(
                "filter" => FILTER_VALIDATE_INT,
                "flags" => FILTER_REQUIRE_ARRAY
            )
        );
        $arrInputVal = filter_input_array(INPUT_POST, $args);


        // Save user related groups if any
        if (count($arrInputVal["groups"])) {
            $arrGroups = array_keys($arrInputVal["groups"]);

            foreach ($arrGroups as $value) {
                $params = array($iUserID, $value);
                $strInsert = "INSERT INTO usergrouprel(iUserID, iGroupID) VALUES(?,?)";
                $db->_query($strInsert, $params);
            }
        }
        header("Location: ?mode=details&iUserID=" . $iUserID);

        break;

}
