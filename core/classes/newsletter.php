<?php

/**
 * Created City PhpStorm.
 * newsletter: tn116
 * Date: 17-05-2017
 * Time: 09:21
 */
class newsletter
{
    public $db;
    public $iLetterID;
    public $vcEmail;
    public $vcName;
    public $iDeleted;

    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iLetterID" => "ID",
            "vcEmail" => "Email",
            "vcName" => "Name",


        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iLetterID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "vcEmail" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcName" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),


        );

        $this->arrValues = array();
    }

    /**
     * function to get list of newslettersR
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM newsletter WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }


    /**
     * function to get a single newsletter
     * @param $iLetterID
     */
    public function getNews($iLetterID) { //set parameter iLetterID to get a single newsletter
        $this->iLetterID = $iLetterID;
        $sql = "SELECT * FROM newsletter WHERE iLetterID = ? AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iLetterID));
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }
        //showme($row);
    }


    /**
     * @return int
     *
     */
    public function save() {
        if ($this->iLetterID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcEmail,
                $this->vcName,
                $this->iLetterID,
            );

            $sql = "UPDATE newsletter SET " .
                "vcEmail = ?, " .
                "vcName = ?, " .
                "WHERE iLetterID = ? ";

            $this->db->_query($sql, $params);
            return $this->iLetterID;


        } else {
            //CREATE MODE
            $params = array(
                $this->vcEmail,
                $this->vcName,

            );

            $sql = "INSERT INTO newsletter (" .
                "vcEmail, " .
                "vcName) " .
                "VALUES(?,?)";
            showme($_POST);
            //exit();
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    public function delete() {
        $params = array($this->iLetterID);

        $sql = "UPDATE newsletter set " .
            "iDeleted = 1 " .
            "WHERE iLetterID = ? ";
        $this->db->_query($sql, $params);

    }

}

