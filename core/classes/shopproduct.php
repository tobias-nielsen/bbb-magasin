<?php

class shopproduct
{
    public $db;
    public $arrLabels;
    public $arrFormElms;
    public $arrValues;
    public $iProductID;
    public $vcProductNumber;
    public $vcTitle;
    public $txShortDesc;
    public $txLongDesc;
    public $txSpecifications;
    public $vcImage1;
    public $vcImage2;
    public $vcImage3;
    public $iPrice;
    public $iOfferPrice;
    public $iStock;
    public $iWeight;
    public $daCreated;
    public $iIsActive;
    public $iSortNum;
    public $iDeleted;


    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iProductID" => "ID",
            "vcProductNumber" => "Produkt Nummer",
            "vcTitle" => "Produkt Navn",
            "txShortDesc" => "Short Description",
            "txLongDesc" => "Long Description",
            "txSpecifications" => "Specs",
            "vcImage1" => "Billede 1",
            "vcImage2" => "Billede 2",
            "vcImage3" => "Billede 3",
            "iPrice" => "Pris",
            "iOfferPrice" => "Tilbuds Pris",
            "iStock" => "Lagerbeholdning",
            "iWeight" => "Vægt",
            "daCreated" => "Oprettet",
            "iIsActive" => "Active",
            "iSortNum" => "Sortering",

        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iProductID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "vcProductNumber" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcTitle" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "txShortDesc" => array("textarea", FILTER_SANITIZE_STRING, TRUE, ""),
            "txLongDesc" => array("textarea", FILTER_SANITIZE_STRING, FALSE, ""),
            "txSpecifications" => array("textarea", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcImage1" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "vcImage2" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "vcImage3" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "iPrice" => array("number", FILTER_VALIDATE_INT, TRUE, ""),
            "iOfferPrice" => array("number", FILTER_VALIDATE_INT, FALSE, ""),
            "iStock" => array("number", FILTER_VALIDATE_INT, TRUE, ""),
            "iWeight" => array("number", FILTER_VALIDATE_INT, TRUE, ""),
            "daCreated" => array("hidden", FILTER_VALIDATE_INT, FALSE, ""),
            "iIsActive" => array("number", FILTER_VALIDATE_INT, FALSE, ""),
            "iSortNum" => array("number", FILTER_VALIDATE_INT, FALSE, ""),


        );

        $this->arrValues = array();
    }


    /**
     * Function to sort products City date created
     * @return array
     */

    public function arrivals() {
        $sql = "SELECT * FROM shopproduct " .
            "WHERE iDeleted = 0 ORDER BY daCreated DESC limit 6 ";
        return $this->db->_fetch_array($sql);

    }

    /**
     *Random Product Slider SQL call
     * @return array
     * RAND() = randomizes order
     * Limit = put limit on number of elements pulled out
     */
    public function productSlide() {  //function in class = method
        $sql = "SELECT * FROM shopproduct " .
            "WHERE iDeleted = 0 ORDER BY RAND() limit 5";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.
    }

    /**
     * function to get list of products
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM shopproduct WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }

    /**
     * function to get a single shopproduct
     * @param $iProductID
     * @return array
     */
    public function getProduct($iProductID) { //set parameter iProductID to get a single shopproduct
        $this->iProductID = $iProductID;
        $sql = "SELECT * FROM shopproduct WHERE iProductID = ? AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iProductID));

        foreach ($row[0] as $key => $value) {
            $this->$key = $value;

        }

        return $row;
    }


    /**
     * Get product related groups
     * @return array
     */

    public function getGroupRelations() {
        $params = array($this->arrValues["iProductID"]);
        $strSelect = "SELECT c.iCategoryID, c.vcTitle " .
            " FROM shopcategory c " .
            "JOIN shopcatprodrel x ON x.iCategoryID = c.iCategoryID " .
            "WHERE x.iProductID = ? " .
            "AND c.iDeleted = 0";
        return $this->db->_fetch_array($strSelect, $params);
    }


    /**
     * Save item
     */
    public function save() {
        if ($this->iProductID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcProductNumber,
                $this->vcTitle,
                $this->txShortDesc,
                $this->txLongDesc,
                $this->txSpecifications,
                $this->vcImage1,
                $this->vcImage2,
                $this->vcImage3,
                $this->iPrice,
                $this->iOfferPrice,
                $this->iStock,
                $this->iWeight,
                $this->daCreated,
                $this->iIsActive,
                $this->iSortNum,
            );

            $sql = "UPDATE shopproduct SET " .
                "vcProductNumber = ?, " .
                "vcTitle = ?, " .
                "txShortDesc = ?, " .
                "txLongDesc = ?, " .
                "txSpecifications = ?, " .
                "vcImage1 = ?, " .
                "vcImage2 = ?, " .
                "vcImage3 = ?, " .
                "iPrice = ?, " .
                "iOfferPrice = ?, " .
                "iStock = ?, " .
                "iWeight = ?," .
                "daCreated = ?, " .
                "iIsActive = ?," .
                "iSortNum = ?" .
                "WHERE iProductID = ? ";

            $this->db->_query($sql, $params);
            return $this->iProductID;

        } else {
            //CREATE MODE
            $params = array(
                $this->vcProductNumber,
                $this->vcTitle,
                $this->txShortDesc,
                $this->txLongDesc,
                $this->txSpecifications,
                $this->vcImage1,
                $this->vcImage2,
                $this->vcImage3,
                $this->iPrice,
                $this->iOfferPrice,
                $this->iStock,
                $this->iWeight,
                time(),
                $this->iIsActive,
                $this->iSortNum,
            );

            $sql = "INSERT INTO shopproduct (" .
                "vcProductNumber, " .
                "vcTitle, " .
                "txShortDesc, " .
                "txLongDesc, " .
                "txSpecifications, " .
                "vcImage1, " .
                "vcImage2, " .
                "vcImage3, " .
                "iPrice, " .
                "iOfferPrice, " .
                "iStock, " .
                "iWeight, " .
                "daCreated, " .
                "iIsActive, " .
                "iSortNum) " .
                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            showme($_POST);
            //exit();
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    /**
     * Delete item
     */

    public function delete($iItemID) {
        $params = array($this->iProductID);

        $sql = "UPDATE shopproduct set " .
            "iDeleted = 1 " .
            "WHERE iProductID = ? ";
        $this->db->_query($sql, $params);
    }


}