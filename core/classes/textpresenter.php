<?php

class textpresenter
{

    /**
     *
     * If arrButtonPanel is set and is higher than 0. Foreach div + class
     * @param $strModuleName
     * @param $strModuleMode
     * @param $arrButtonPanel
     * @return string
     */
    static function presentpanel($strModuleName, $strModuleMode, $arrButtonPanel) {
        $accHtml = "<div class='mainheader row rowfix'>\n";
        $accHtml .= "<div class='col-xs-7'>\n";
        $accHtml .= "<h1>" . $strModuleName . "</h1>\n";
        $accHtml .= "<h2>" . $strModuleMode . "</h2>\n";
        $accHtml .= "</div>\n";

        if (isset($arrButtonPanel) && count($arrButtonPanel) > 0) {
            $accHtml .= "<div class='pull-right btn-fixer'>\n";
            foreach ($arrButtonPanel as $key => $value) {
                $accHtml .= $value;
            }

            $accHtml .= "</div>\n";
        }

        $accHtml .= "</div>\n";
        return $accHtml;
    }
}